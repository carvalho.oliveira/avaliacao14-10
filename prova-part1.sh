#!/bin/bash

AZUL="\e[1;34m"
AZULL="\e[0;34m"
END="\e[0m"

echo -e "${AZUL}==============================================================================${END}"
echo -e "${AZUL}\t \t \t \tMENU DE OPÇÕES:${END}"
echo -e "${AZUL}==============================================================================${END}"
echo -e "${AZULL}a)exibe a quantidade de espaço de disco usado${END}"
echo -e "${AZULL}b)exibe a quantidade total de memória${END}"
echo -e "${AZULL}c)verifica se um arquivo existe (se o resultado for = 0, o arquivo existe)${END}"
echo -e "${AZULL}d)exibe o diretório atual${END}"
echo -e "${AZULL}e)lista os arquivos do diretório atual${END}"
echo -e "${AZULL}f)muda o diretório atual${END}"
echo -e "${AZULL}sair) encerra o laço de repetição${END}"
echo -e "${AZUL}==============================================================================${END}"

while [ "${opc}" != "sair" ]; do
	read opc 
	if [ "${opc}" == "a" ]; then
		df -h
		sleep 2
		
	elif [ "${opc}" == "b" ]; then
		free -t
		sleep 2

	elif [ "${opc}" == "c" ]; then
	        > a.txt	
		test -f a.txt; echo $?
		sleep 2
	
	elif [ "${opc}" == "d" ]; then
		pwd 
		sleep 2
	
	elif [ "${opc}" == "e" ]; then
		ls
		sleep 2

	elif [ "${opc}" == "f" ]; then
		cd ..
		echo "novo diretótio: $(pwd)"
		sleep 2
	else
		echo "opção inválida!"
	fi
done
